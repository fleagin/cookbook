\changetocdepth {1}
\contentsline {chapter}{Table of Contents}{i}{section*.1}%
\contentsline {chapter}{Breakfast}{1}{chapter*.2}%
\contentsline {section}{\numberline {0.1}Belgian Waffles}{2}{section.0.1}%
\contentsline {subsection}{Belgian Waffles}{2}{r@cipenumber.1}%
\contentsline {section}{\numberline {0.2}Buttermilk Pancakes}{3}{section.0.2}%
\contentsline {subsection}{Buttermilk Pancakes}{3}{r@cipenumber.2}%
\contentsline {chapter}{Chicken}{5}{chapter*.3}%
\contentsline {section}{\numberline {0.3}Buttermilk Fried Chicken}{6}{section.0.3}%
\contentsline {subsection}{Buttermilk Fried Chicken}{6}{r@cipenumber.3}%
\contentsline {section}{\numberline {0.4}General Tso's Chicken}{8}{section.0.4}%
\contentsline {subsection}{General Tso's Chicken}{8}{r@cipenumber.4}%
\contentsline {section}{\numberline {0.5}Grilled Chicken}{10}{section.0.5}%
\contentsline {subsection}{Grilled Chicken}{10}{r@cipenumber.5}%
\contentsline {section}{\numberline {0.6}Nashville Hot Chicken Sandwich}{11}{section.0.6}%
\contentsline {subsection}{Nashville Hot Chicken Sandwich}{11}{r@cipenumber.6}%
\contentsline {section}{\numberline {0.7}Sichuan Mala Chicken (辣子鸡, La Zi Ji)}{13}{section.0.7}%
\contentsline {subsection}{Sichuan Mala Chicken (辣子鸡, La Zi Ji)}{13}{r@cipenumber.7}%
\contentsline {chapter}{Fish}{15}{chapter*.4}%
\contentsline {section}{\numberline {0.8}Honey Garlic Salmon}{16}{section.0.8}%
\contentsline {subsection}{Honey Garlic Salmon}{16}{r@cipenumber.8}%
\contentsline {chapter}{Noodles \& Pasta}{17}{chapter*.5}%
\contentsline {section}{\numberline {0.9}Baked Ziti}{18}{section.0.9}%
\contentsline {subsection}{Baked Ziti}{18}{r@cipenumber.9}%
\contentsline {section}{\numberline {0.10}Five Cheese Mac \& Cheese}{20}{section.0.10}%
\contentsline {subsection}{Five Cheese Mac \& Cheese}{20}{r@cipenumber.10}%
\contentsline {section}{\numberline {0.11}Pad Thai}{21}{section.0.11}%
\contentsline {subsection}{Pad Thai}{21}{r@cipenumber.11}%
\contentsline {chapter}{Pizza}{23}{chapter*.6}%
\contentsline {section}{\numberline {0.12}Detroit Style Pizza}{24}{section.0.12}%
\contentsline {subsection}{Detroit Style Pizza}{24}{r@cipenumber.12}%
\contentsline {chapter}{Soup}{25}{chapter*.7}%
\contentsline {section}{\numberline {0.13}Beef Phở}{26}{section.0.13}%
\contentsline {subsection}{Beef Phở}{26}{r@cipenumber.13}%
\contentsline {section}{\numberline {0.14}Chicken Noodle Soup}{28}{section.0.14}%
\contentsline {subsection}{Chicken Noodle Soup}{28}{r@cipenumber.14}%
\contentsline {section}{\numberline {0.15}French Onion Soup}{30}{section.0.15}%
\contentsline {subsection}{French Onion Soup}{30}{r@cipenumber.15}%
\contentsline {chapter}{Sides}{31}{chapter*.8}%
\contentsline {section}{\numberline {0.16}Elote}{32}{section.0.16}%
\contentsline {subsection}{Elote}{32}{r@cipenumber.16}%
\contentsline {section}{\numberline {0.17}Garlic Butter Rice}{33}{section.0.17}%
\contentsline {subsection}{Garlic Butter Rice}{33}{r@cipenumber.17}%
\contentsline {chapter}{Bread}{35}{chapter*.9}%
\contentsline {section}{\numberline {0.18}Golden Cheddar Loaf}{36}{section.0.18}%
\contentsline {subsection}{Golden Cheddar Loaf}{36}{r@cipenumber.18}%
\contentsline {chapter}{Desserts}{37}{chapter*.10}%
\contentsline {section}{\numberline {0.19}Blueberry Pie}{38}{section.0.19}%
\contentsline {subsection}{Blueberry Pie}{38}{r@cipenumber.19}%
\contentsline {section}{\numberline {0.20}Brown Butter \& Toffee Chocolate Chip Cookies}{40}{section.0.20}%
\contentsline {subsection}{Brown Butter \& Toffee Chocolate Chip Cookies}{40}{r@cipenumber.20}%
\contentsline {section}{\numberline {0.21}Chocolate Lava Cake}{41}{section.0.21}%
\contentsline {subsection}{Chocolate Lava Cake}{41}{r@cipenumber.21}%
\contentsline {section}{\numberline {0.22}Cinnamon Rolls}{42}{section.0.22}%
\contentsline {subsection}{Cinnamon Rolls}{42}{r@cipenumber.22}%
\contentsline {section}{\numberline {0.23}Cream Cheese Chocolate Tube Cake}{43}{section.0.23}%
\contentsline {subsection}{Cream Cheese Chocolate Tube Cake}{43}{r@cipenumber.23}%
\contentsline {section}{\numberline {0.24}Flakiest Pie Crust}{44}{section.0.24}%
\contentsline {subsection}{Flakiest Pie Crust}{44}{r@cipenumber.24}%
\contentsline {section}{\numberline {0.25}Strawberry-Lemon Lattice Pie}{45}{section.0.25}%
\contentsline {subsection}{Strawberry-Lemon Lattice Pie}{45}{r@cipenumber.25}%
\contentsline {chapter}{Sauces, Rubs \& Marinades}{47}{chapter*.12}%
\contentsline {section}{\numberline {0.26}Basic Barbecue Pork Rub}{48}{section.0.26}%
\contentsline {subsection}{Basic Barbecue Pork Rub}{48}{r@cipenumber.26}%
\contentsline {section}{\numberline {0.27}Cajun Rub}{48}{section.0.27}%
\contentsline {subsection}{Cajun Rub}{48}{r@cipenumber.27}%

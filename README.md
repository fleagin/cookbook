This is the repo for my cookbook. It is typeset in LaTeX as a **memoir** using the **cuisine** package. I have made some custom pagestyles within **memoir** that are used throughout.

Put the "Images" directory in the same directory as Cookbook.tex and compile with **XeLaTex** or just take the PDF.

Some of the recipes are my own, some have been pulled from public sources. The same goes for images.
